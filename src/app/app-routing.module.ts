import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { adminGuard } from './core/guard/admin.guard';
import { authGuard } from './core/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'doctor',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'users',
    canActivate: [adminGuard],
    loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule),
  },
  {
    path: 'dashboard',
    canActivate: [authGuard],
    loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'settings',
    canActivate: [adminGuard],
    loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: 'denied',
    loadChildren: () => import('./features/denied/denied.module').then(m => m.DeniedModule)
  },
  {
    path: 'not-found',
    loadChildren: () => import('./features/not-found/not-found.module').then(m => m.NotFoundModule)
  },
  {
    path: 'nurse',
    canActivate: [adminGuard],
    loadChildren: () => import('./features/nurse/nurse.module').then(m => m.NurseModule)
  },
  {
    path: 'doctor',
    canActivate: [adminGuard],
    loadChildren: () => import('./features/doctor/doctor.module').then(m => m.DoctorModule),
    data: {
      breadcrumb: 'หน้าหลัก'
    },
  },
  { path: 'test', loadChildren: () => import('./features/test/test.module').then(m => m.TestModule) },
  { path: 'progressnote', loadChildren: () => import('./features/doctor/progressnote/progressnote.module').then(m => m.ProgressnoteModule) },
  { path: 'standing', loadChildren: () => import('./features/doctor/standing/standing.module').then(m => m.StandingModule) },
  { path: 'laboratory', loadChildren: () => import('./features/doctor/laboratory/laboratory.module').then(m => m.LaboratoryModule) },
  // {
  //   path: 'listward',
  //   canActivate: [adminGuard],
  //   loadChildren: () => import('./features/doctor/listward/listward.module').then(m => m.ListwardModule)
  // },
  // {
  //   path: 'patientinfo',
  //   canActivate: [adminGuard],
  //   loadChildren: () => import('./features/doctor/patientinfo/patientinfo.module').then(m => m.PatientinfoModule)
  // },  
  // {
  //   path: 'admissionnote',
  //   canActivate: [adminGuard],
  //   loadChildren: () => import('./features/doctor/admissionnote/admissionnote.module').then(m => m.AdmissionnoteModule)
  // },
  // {
  //   path: 'listpatient',
  //   canActivate: [adminGuard],
  //   loadChildren: () => import('./features/doctor/listpatient/listpatient.module').then(m => m.ListpatientModule)
  // },

  { path: '**', redirectTo: 'not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
