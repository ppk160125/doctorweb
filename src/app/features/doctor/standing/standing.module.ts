import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StandingRoutingModule } from './standing-routing.module';
import { StandingComponent } from './standing.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    StandingComponent
  ],
  imports: [
    NgZorroModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    StandingRoutingModule
  ]
})
export class StandingModule { }
