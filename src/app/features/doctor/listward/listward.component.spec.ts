import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListwardComponent } from './listward.component';

describe('ListwardComponent', () => {
  let component: ListwardComponent;
  let fixture: ComponentFixture<ListwardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListwardComponent]
    });
    fixture = TestBed.createComponent(ListwardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
