import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListwardComponent } from './listward.component';

const routes: Routes = [
  { 
    path: '', 
    component: ListwardComponent,
    data: {
      breadcrumb: 'รายการตึก'
    },
    
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListwardRoutingModule { }
