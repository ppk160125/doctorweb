import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { AxiosResponse } from 'axios';
import { NotificationService } from '../../../shared/services/notification.service';
import { LibService } from '../../../shared/services/lib.service';
import { NgxSpinnerService } from "ngx-spinner";
// import {DeviceCheckComponent} from './app-device-check';
import {VariableShareService}  from '../../../core/services/variable-share.service';


@Component({
  selector: 'app-listward',
  templateUrl: './listward.component.html',
  styleUrls: ['./listward.component.css']
})
export class ListwardComponent {

  query: any = '';
  dataSet: any[] = [];
  loading = false;
  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name:any;
  item:any=[1,2,3,4,5,6,7,8,9];
  wards: any = [];


  constructor (
    private router: Router,
    private notificationService:NotificationService,
    private libService:LibService,
    private spinner: NgxSpinnerService,
    private nzMessageService: NzMessageService,
    private variableShareService:VariableShareService

  ) {
    this.user_login_name  =  sessionStorage.getItem('userLoginName'); 
   }

  ngOnInit(): void {    
    this.getWard();

  }
  /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }

  closeSidebar(): void {
    this.variableShareService.setCloseSidebar(true);
  }
  cancel(): void {
    this.nzMessageService.info('click cancel');
  }

  confirm(): void {
    this.nzMessageService.info('click confirm');
  }

  beforeConfirm(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(true);
      }, 3000);
    });
  }

  //////////////////////
  logOut(){
    sessionStorage.setItem('token','');
    return this.router.navigate(['/login']);    
  }
  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getWard()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getWard()
  }

  navigatePatient(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.closeSidebar();
    this.router.navigate(['/doctor/listpatient'], { queryParams: { data: jsonString } });

  }

  async getWard() {
    this.spinner.show();
    try {
      const response: AxiosResponse = await this.libService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      console.log('Ward : ', this.wards);
      this.hideSpinner();
    } catch (error: any) {
      console.log(error);
      this.hideSpinner();
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

}
