import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorComponent } from './doctor.component';
import { nurseWaitingGuard } from '../../core/guard/nurse-waiting.guard';

const routes: Routes = [
  {
    path: '',
    component: DoctorComponent,
    children: [
      {
        path: '', redirectTo: 'listward', pathMatch: 'full'
      },
      {
        path: 'main',
        loadChildren: () => import('./main/main.module').then(m => m.MainModule),
        data: {
          breadcrumb: 'ทั่วไป'
        },
      },
      {
        path: 'listward',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./listward/listward.module').then(m => m.ListwardModule),
        data: {
          breadcrumb: 'ตึกผู้ป่วยใน'
        },
      },{
        path: 'patientinfo',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./patientinfo/patientinfo.module').then(m => m.PatientinfoModule)
      }
      ,{
        path: 'admissionnote',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./admissionnote/admissionnote.module').then(m => m.AdmissionnoteModule)
      },
      {
        path: 'listpatient',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./listpatient/listpatient.module').then(m => m.ListpatientModule),
        data: {
          breadcrumb: 'ทะเบียนผู้ป่วยใน'
        },
      },{
        path: 'doctor-order',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./doctor-order/doctor-order.module').then(m => m.DoctorOrderModule)
      },{
        path: 'progressnote',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./progressnote/progressnote.module').then(m => m.ProgressnoteModule)
      },{
        path: 'standing',
        canActivate: [nurseWaitingGuard],
        loadChildren: () => import('./standing/standing.module').then(m => m.StandingModule)
      },
      
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
