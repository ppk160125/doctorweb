import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import {UserProfileService} from '../../../core/services/user-profiles.service';

@Component({
  selector: 'app-doctor-order',
  templateUrl: './doctor-order.component.html',
  styleUrls: ['./doctor-order.component.css']
})
export class DoctorOrderComponent {
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name:any;

  progressValue?: string;
  onedayValue?: string;
  continueValue?: string;

  progressTags = ['progressValue'];
  onedayTags = ['onedayValue'];
  continueTags = ['onedayValue'];

  inputVisible = false;
  inputValue?: string ='';
  @ViewChild('inputElement', { static: false }) inputElement?: ElementRef;

  constructor (
    private router: Router,
    private doctorService: DoctorService,
    private message: NzMessageService,
    private modal: NzModalService,
    private userProfileService:UserProfileService,

  ) {
    this.user_login_name  = this.userProfileService.fname;

   }

  ngOnInit(): void {
    // this.user_login_name  =  this.userProfileService.user_login_name;
    this.user_login_name  =  sessionStorage.getItem('userLoginName'); 


  }
  logOut(){
    sessionStorage.setItem('token','');
    return this.router.navigate(['/login']);    
  }

  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.doctorService.getWaiting(_limit, _offset);

      const data: any = response.data;

      this.total = data.total || 1

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS) : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  preventDefault(e: Event): void {
    e.preventDefault();
    e.stopPropagation();
    console.log('tag can not be closed.');
  }

  handleCloseProgress(removedTag: {}): void {
    this.progressTags = this.progressTags.filter(progressTags => progressTags !== removedTag);
  }

  sliceTagNameProgress(progressTags: string): string {
    const isLongTag = progressTags.length > 20;
    return isLongTag ? `${progressTags.slice(0, 20)}...` : progressTags;
  }

  handleCloseOneday(removedTag: {}): void {
    this.onedayTags = this.onedayTags.filter(onedayTags => onedayTags !== removedTag);
  }

  sliceTagNameOneday(onedayTags: string): string {
    const isLongTag = onedayTags.length > 20;
    return isLongTag ? `${onedayTags.slice(0, 20)}...` : onedayTags;
  }

  handleCloseContinue(removedTag: {}): void {
    this.continueTags = this.continueTags.filter(continueTags => continueTags !== removedTag);
  }

  sliceTagNameContinue(continueTags: string): string {
    const isLongTag = continueTags.length > 20;
    return isLongTag ? `${continueTags.slice(0, 20)}...` : continueTags;
  }

  showInput(): void {
    this.inputVisible = true;
    setTimeout(() => {
      this.inputElement?.nativeElement.focus();
    }, 10);
  }

  // handleInputConfirm(): void {
  //   if (this.inputValue && this.tags.indexOf(this.inputValue) === -1) {
  //     this.tags = [...this.tags, this.inputValue];
  //   }
  //   this.inputValue = '';
  //   this.inputVisible = false;
  // }

  addProgressNote(): void {
    if (this.progressValue && this.progressTags.indexOf(this.progressValue) === -1) {
      this.progressTags = [...this.progressTags, this.progressValue];
    }
    this.progressValue = '';
  }

  addOneday(): void {
    if (this.onedayValue && this.onedayTags.indexOf(this.onedayValue) === -1) {
      this.onedayTags = [...this.onedayTags, this.onedayValue];
    }
    this.onedayValue = '';
  }

  addContinue(): void {
    if (this.continueValue && this.continueTags.indexOf(this.continueValue) === -1) {
      this.continueTags = [...this.continueTags, this.continueValue];
    }
    this.continueValue = '';
  }

}

