import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorOrderComponent } from './doctor-order.component';

describe('DoctorOrderComponent', () => {
  let component: DoctorOrderComponent;
  let fixture: ComponentFixture<DoctorOrderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DoctorOrderComponent]
    });
    fixture = TestBed.createComponent(DoctorOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
