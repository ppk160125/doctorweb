import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientinfoComponent } from './patientinfo.component';

const routes: Routes = [{ path: '', component: PatientinfoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientinfoRoutingModule { }
