import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListpatientComponent } from './listpatient.component';

const routes: Routes = [{ path: '', component: ListpatientComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListpatientRoutingModule { }
