import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListpatientRoutingModule } from './listpatient-routing.module';
import { ListpatientComponent } from './listpatient.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {DeviceCheckComponent} from './app-device-check';






@NgModule({
  declarations: [
    ListpatientComponent,DeviceCheckComponent
  ],
  imports: [
    NgZorroModule,
    SharedModule,
    CommonModule,
    ListpatientRoutingModule,DragDropModule
  ]
})
export class ListpatientModule { }
